#include <catch.hpp>

#include "../telegram/fake.h"
#include "../telegram/Bot.h"

#include <Poco/Exception.h>
#include <Poco/Net/NetException.h>
#include <iostream>

constexpr auto token = "123";

TEST_CASE("Single getMe") {
  telegram::FakeServer server("Single getMe");
  server.Start();

  auto url = server.GetUrl();
  Bot bot(url, token, "Test BotApi", "botStateTest1");

  try {
    bot.GetMe();

  } catch(Poco::Exception& exception) {
    std::cerr << "Poco exception occured: " << exception.displayText() << std::endl;

  } catch(std::exception& exception) {
    std::cerr << "std exception occured: " << exception.what() << std::endl;

  } catch(...) {
    std::cerr << "unknown exception\n";
  }

  server.StopAndCheckExpectations();
}

TEST_CASE("getMe error handling") {
  telegram::FakeServer server("getMe error handling");
  server.Start();

  auto url = server.GetUrl();
  Bot bot(url, token, "Test BotApi", "botStateTest2");

  try {
    auto user = bot.GetMe();

  } catch (Poco::Net::HTTPException& exception) {
    std::cout << "Exception occured as expected\n";

  } catch(Poco::Exception& exception) {
    std::cerr << "Poco exception occured: " << exception.displayText() << std::endl;

  } catch(std::exception& exception) {
    std::cerr << "std exception occured: " << exception.what() << std::endl;

  } catch(...) {
    std::cerr << "unknown exception\n";
  }

  try {
    auto user = bot.GetMe();

  } catch (Poco::RuntimeException& exception) {
    std::cout << "Exception occured as expected\n";

  } catch(Poco::Exception& exception) {
    std::cerr << "Poco exception occured: " << exception.displayText() << std::endl;

  } catch(std::exception& exception) {
    std::cerr << "std exception occured: " << exception.what() << std::endl;

  } catch(...) {
    std::cerr << "unknown exception\n";
  }

  server.StopAndCheckExpectations();
}

TEST_CASE("Single getUpdates and send messages") {
  telegram::FakeServer server("Single getUpdates and send messages");
  server.Start();

  auto url = server.GetUrl();
  Bot bot(url, token, "Test BotApi", "botStateTest3");

  try {
    auto messages = bot.GetUpdates();
    auto first_chat_id = messages[0].chat.id;
    bot.SendMessage(SendingMessage(first_chat_id, "Hi!"));

    auto second_char_id = messages[1].chat.id;
    bot.SendMessage(SendingMessage(second_char_id, "Reply", 2));
    bot.SendMessage(SendingMessage(second_char_id, "Reply", 2));

  } catch(Poco::Exception& exception) {
    std::cerr << "Poco exception occured: " << exception.displayText() << std::endl;

  } catch(std::exception& exception) {
    std::cerr << "std exception occured: " << exception.what() << std::endl;

  } catch(...) {
    std::cerr << "unknown exception\n";
  }

  server.StopAndCheckExpectations();
}

TEST_CASE("Handle getUpdates offset") {
  telegram::FakeServer server("Handle getUpdates offset");
  server.Start();

  auto url = server.GetUrl();
  Bot bot(url, token, "Test BotApi", "botStateTest4");

  try {
    auto messages1 = bot.GetUpdates(GettingUpdates(5));
    std::cout << messages1.size() << std::endl;
    int64_t max_update_id = 0;
    for (auto& message : messages1) {
      if (max_update_id < message.update_id)
        max_update_id = message.update_id;
    }

    auto messages2 = bot.GetUpdates(GettingUpdates(5, max_update_id + 1));
    std::cout << messages2.size() << std::endl;

    auto messages3 = bot.GetUpdates(GettingUpdates(5, max_update_id + 1));
    std::cout << messages3.size() << std::endl;

  } catch(Poco::Exception& exception) {
    std::cerr << "Poco exception occured: " << exception.displayText() << std::endl;

  } catch(std::exception& exception) {
    std::cerr << "std exception occured: " << exception.what() << std::endl;

  } catch(...) {
    std::cerr << "unknown exception\n";
  }

  server.StopAndCheckExpectations();
}