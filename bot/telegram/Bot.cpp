//
// Created by IVAN MATVEEV on 05.12.2017.
//


#include "Bot.h"
#include <Poco/Exception.h>

int32_t Bot::RandomGenerator::rand() {
  return distribution_(generator_);
}

Bot::RandomGenerator::RandomGenerator() :
  generator_(static_cast<uint32_t>(time(nullptr)))
{}

Bot::Bot(std::string server_url,
         std::string token,
         std::string first_name,
         std::string savingStateFilePath) :
    botApi_(server_url, token),
    working_(true),
    state_(savingStateFilePath, 30),
    first_name_(first_name)
{}

void Bot::Stop() {
  working_ = false;
}

void Bot::Random(const Message& message) {
  SendingMessage sendingMessage(message.chat.id, std::to_string(generator_.rand()));
  botApi_.SendMessage(sendingMessage);
}

void Bot::Weather(const Message &message) {
  botApi_.SendMessage(SendingMessage(message.chat.id, "Winter Is Coming"));
}

void Bot::StyleGuide(const Message &message) {
  std::string text = "I know that I don't know style guide, others don't even know it\n";
  SendingMessage sendingMessage(message.chat.id, text);
  botApi_.SendMessage(sendingMessage);
}

void Bot::Sticker(const Message &message) {
  botApi_.SendMessage(SendingMessage(message.chat.id, "It is sticker!"));
}

void Bot::Crash() {
  throw 0;
}

void Bot::Run() {
  working_ = true;
  while (working_) {
    auto messages = botApi_.GetUpdates(state_.GetGettingUpdate());
    for (const auto& message : messages) {
      state_.SetOffset(message.update_id + 1);
      if (message.text) {
        auto text = *(message.text);
        if (text == "/random") {
          Random(message);
        } else if (text == "/weather") {
          Weather(message);
        } else if (text == "/styleguide") {
          StyleGuide(message);
        } else if (text == "/stop") {
          Stop();
        } else if (text == "/crash") {
          Crash();
        }
      } else if (message.sticker) {
        Sticker(message);
      }
    }
  }
}

User Bot::GetMe() {
  auto user = botApi_.GetMe();
  if (user.first_name != first_name_ || !user.is_bot)
    throw Poco::Exception("first_names isn't equal or is_bot = false\n");
  return user;
}

std::vector<Message> Bot::GetUpdates() {
  return botApi_.GetUpdates(GettingUpdates());
}

std::vector<Message> Bot::GetUpdates(GettingUpdates update) {
  return botApi_.GetUpdates(update);
}

void Bot::SendMessage(const SendingMessage &message) {
  botApi_.SendMessage(message);
}

void Bot::StateStorage::SetOffset(int64_t offset) {
  update_.offset.reset(new int64_t(offset));
  file_.seekg(0);
  file_ << offset;
  file_.flush();
  if (file_.fail())
    throw Poco::Exception("can't write to file for saving bot state\n");
}

const GettingUpdates& Bot::StateStorage::GetGettingUpdate() {
  return update_;
}

Bot::StateStorage::StateStorage(std::string filePath, int64_t timeout) :
    file_(filePath, std::fstream::out | std::fstream::in)
{
  update_.timeout.reset(new int64_t(timeout));

  if (file_.fail()) {
    file_.open(filePath, std::fstream::out);

    if (file_.fail())
      throw Poco::Exception("can't open '" + filePath + "' for saving bot state\n");
  } else {
    int64_t offset;
    file_ >> offset;
    update_.offset.reset(new int64_t(offset));
  }
}

Bot::StateStorage::~StateStorage() {
  file_.close();
}
