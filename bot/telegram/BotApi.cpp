//
// Created by IVAN MATVEEV on 03.12.2017.
//

#include "BotApi.h"
#include "JsonManager.h"

#include <Poco/Net/HTTPSClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/HTTPMessage.h>
#include <Poco/URI.h>
#include <json/json.h>
#include <iostream>


using namespace Poco;
using namespace Net;

class BotApi::BotApiImpl {
public:
  explicit BotApiImpl(std::string server_url, std::string token);
  ~BotApiImpl();
  User GetMe();
  std::vector<Message> GetUpdates(const GettingUpdates& update);
  void SendMessage(const SendingMessage& message);

private:
  std::string url_;
  std::unique_ptr<HTTPClientSession> session_;

  std::string BuildUpdateUrl(const GettingUpdates& update);
};


BotApi::BotApiImpl::BotApiImpl(std::string server_url, std::string token) :
    url_(server_url + "bot" + token + "/")
{
  URI uri(server_url);
  if (server_url.substr(0, 5) == "https") {
    session_ = std::make_unique<HTTPSClientSession>(uri.getHost(), uri.getPort());
  } else if (server_url.substr(0, 4) == "http") {
    session_ = std::make_unique<HTTPClientSession>(uri.getHost(), uri.getPort());
  } else {
    throw Exception("Bad url\n");
  }
}

BotApi::BotApiImpl::~BotApiImpl() {
}

User BotApi::BotApiImpl::GetMe() {
  URI uri(url_ + "getMe");
  HTTPRequest request(HTTPRequest::HTTP_GET, uri.getPathAndQuery(), HTTPMessage::HTTP_1_1);
  session_->sendRequest(request);

  HTTPResponse response;
  auto& receiveBody = session_->receiveResponse(response);

  if (response.getStatus() != HTTPResponse::HTTP_OK)
    throw RuntimeException("in getMe occur error: response.getStatus() = " +
                    std::to_string(response.getStatus()) + "\n");

  Json::Value jsonValue;
  receiveBody >> jsonValue;

  Json::Value result = jsonValue["result"];

  if (result.isNull())
    throw RuntimeException("in json no field result in getMe\n");

  return JsonManager::readUser(result);
}

std::vector<Message> BotApi::BotApiImpl::GetUpdates(const GettingUpdates& update) {
  auto url(BuildUpdateUrl(update));
  URI uri(url);
  HTTPRequest request(HTTPRequest::HTTP_GET, uri.getPathAndQuery(), HTTPMessage::HTTP_1_1);

  auto& reqstream = session_->sendRequest(request);

  HTTPResponse response;
  auto& receiveBody = session_->receiveResponse(response);
  if (response.getStatus() != HTTPResponse::HTTP_OK)
    throw RuntimeException("in GetUpdates occur error: response.getStatus() = " +
                    std::to_string(response.getStatus()) + "\n");

  Json::Value receivingJson;
  receiveBody >> receivingJson;
  std::cout << receivingJson.toStyledString() << "\n";

  Json::Value result = receivingJson["result"];

  if (result.isNull())
    throw RuntimeException("in json no field result in getUpdates\n");

  return JsonManager::readArrayMessage(result);
}

void BotApi::BotApiImpl::SendMessage(const SendingMessage& message) {
  URI uri(url_ + "sendMessage");
  HTTPRequest request(HTTPRequest::HTTP_POST, uri.getPathAndQuery(), HTTPMessage::HTTP_1_1);

  Json::Value sendingJson = JsonManager::messageToJson(message);
  std::cout << sendingJson.toStyledString() << "\n";

  request.setContentType("application/json");
  request.setContentLength(sendingJson.toStyledString().size());

  auto& reqstream = session_->sendRequest(request);
  reqstream << sendingJson.toStyledString();

  HTTPResponse response;
  auto& receiveBody = session_->receiveResponse(response);
  if (response.getStatus() != HTTPResponse::HTTP_OK)
    throw RuntimeException("in GetUpdates occur error: response.getStatus() = " +
                    std::to_string(response.getStatus()) + "\n");

  Json::Value receivingJson;
  receiveBody >> receivingJson;
  std::cout << receivingJson.toStyledString() << "\n";
}

std::string BotApi::BotApiImpl::BuildUpdateUrl(const GettingUpdates& update) {
  std::string updateUrl(url_ + "getUpdates");
  bool first = true;
  if (update.offset) {
    std::string delimiter = (first) ? "?" : "&";
    updateUrl += delimiter + "offset=" + std::to_string(*(update.offset));
    first = false;
  }
  if (update.timeout) {
    std::string delimiter = (first) ? "?" : "&";
    updateUrl += delimiter + "timeout=" + std::to_string(*(update.timeout));
    first = false;
  }
  if (update.limit) {
    std::string delimiter = (first) ? "?" : "&";
    updateUrl += delimiter + "limit=" + std::to_string(*(update.limit));
    first = false;
  }
  return updateUrl;
}

BotApi::BotApi(std::string server_url, std::string token) :
  pimpl(new BotApiImpl(server_url, token))
{}

BotApi::~BotApi() {
}

User BotApi::GetMe() {
  return pimpl->GetMe();
}

std::vector<Message> BotApi::GetUpdates(const GettingUpdates& update) {
  return pimpl->GetUpdates(update);
}

void BotApi::SendMessage(const SendingMessage& message) {
  pimpl->SendMessage(message);
}


SendingMessage::SendingMessage(int64_t in_chat_id, std::string in_text) :
    chat_id(in_chat_id), text(in_text)
{}

SendingMessage::SendingMessage(int64_t in_chat_id, std::string in_text, int64_t reply) :
    chat_id(in_chat_id), text(in_text), reply_to_message_id(new int64_t(reply))
{}

GettingUpdates::GettingUpdates(int64_t in_timeout) :
    timeout(new int64_t(in_timeout))
{}

GettingUpdates::GettingUpdates(int64_t in_timeout, int64_t in_offset) :
  offset(new int64_t(in_offset)),
  timeout(new int64_t(in_timeout))
{}

GettingUpdates::GettingUpdates(int64_t in_timeout, int64_t in_offset, int64_t in_limit) :
    offset(new int64_t(in_offset)),
    timeout(new int64_t(in_timeout)),
    limit(new int64_t(in_limit))
{}

