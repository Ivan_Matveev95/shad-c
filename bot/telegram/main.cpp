
#include <iostream>
#include <Poco/Exception.h>

#include "BotApi.h"
#include "Bot.h"

int main() {
  std::string token("479220100:AAFm1RabZWiocKoCgrSz6Lw6fWv33gJfrVg");
  std::string server_url("https://api.telegram.org/");
  Bot bot(server_url, token, "my_bot", "botState");
  try {
    bot.Run();
  } catch (Poco::Exception exception) {
    std::cout << exception.displayText() << "\n";
    exit(-1);
  }

  return 0;
}

/*
int old_main() {
  std::cout << "Hello!" << std::endl;
  std::string token("479220100:AAFm1RabZWiocKoCgrSz6Lw6fWv33gJfrVg");

  Poco::URI uri("https://api.telegram.org/bot479220100:AAFm1RabZWiocKoCgrSz6Lw6fWv33gJfrVg/getMe");
  Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());
  Poco::Net::HTTPRequest request(
      Poco::Net::HTTPRequest::HTTP_GET,
      uri.getPathAndQuery(),
      Poco::Net::HTTPMessage::HTTP_1_1);

  session.sendRequest(request);

  Poco::Net::HTTPResponse response;
  auto& receiveBody = session.receiveResponse(response);
  std::cout << response.getStatus() << " " << response.getReason() << std::endl;
  Json::Value message;
  receiveBody >> message;
  std::cout << message.toStyledString() << "\n";

  {
    Poco::URI uri("https://api.telegram.org/bot479220100:AAFm1RabZWiocKoCgrSz6Lw6fWv33gJfrVg/getUpdates?timeout=1");
    Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());
    Poco::Net::HTTPRequest request(
        Poco::Net::HTTPRequest::HTTP_GET,
        uri.getPathAndQuery(),
        Poco::Net::HTTPMessage::HTTP_1_1);

    auto& reqstream = session.sendRequest(request);

    Poco::Net::HTTPResponse response;
    auto& receiveBody = session.receiveResponse(response);
    std::cout << response.getStatus() << " " << response.getReason() << std::endl;
    Json::Value message;
    receiveBody >> message;
    std::cout << message.toStyledString() << "\n";
  }

  {
    Poco::URI uri("https://api.telegram.org/bot479220100:AAFm1RabZWiocKoCgrSz6Lw6fWv33gJfrVg/sendMessage");
    Poco::Net::HTTPSClientSession session(uri.getHost(), uri.getPort());
    Poco::Net::HTTPRequest request(
        Poco::Net::HTTPRequest::HTTP_POST,
        uri.getPathAndQuery(),
        Poco::Net::HTTPMessage::HTTP_1_1);

    Json::Value value;
    value["chat_id"] = 353770454;
    value["text"] = "blablabla!";

    request.setContentType("application/json");
    request.setContentLength(value.toStyledString().size());

    auto& reqstream = session.sendRequest(request);
    reqstream << value.toStyledString();

    Poco::Net::HTTPResponse response;
    auto& receiveBody = session.receiveResponse(response);
    std::cout << response.getStatus() << " " << response.getReason() << std::endl;
    Json::Value message;
    receiveBody >> message;
    std::cout << message.toStyledString() << "\n";
  }

  return 0;
}
*/