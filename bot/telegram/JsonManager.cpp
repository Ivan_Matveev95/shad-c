//
// Created by IVAN MATVEEV on 05.12.2017.
//

#include "JsonManager.h"
#include <Poco/Exception.h>

using namespace Poco;


std::string JsonManager::getString(const Json::Value& value, std::string key) {
  if (value[key.c_str()].isString()) {
    return value[key.c_str()].asString();
  } else {
    throw Exception("in message no field '" + key + "'type string\n");
  }
}

int64_t JsonManager::getInt(const Json::Value& value, std::string key) {
  if (value[key].isInt()) {
    return value[key].asInt();
  } else {
    throw Exception("in message no field '" + key + "'type int\n");
  }
}

bool JsonManager::getBool(const Json::Value& value, std::string key) {
  if (value[key].isBool()) {
    return value[key].asBool();
  } else {
    throw Exception("in message no field '" + key + "' type bool\n");
  }
}

std::unique_ptr<std::string>
JsonManager::getOptionalString(const Json::Value& value, std::string key) {
  if (value[key].isNull())
    return nullptr;

  return std::make_unique<std::string>(getString(value, key));
}

std::unique_ptr<int64_t>
JsonManager::getOptionalInt(const Json::Value& value, std::string key) {
  if (value[key].isNull())
    return nullptr;

  return std::make_unique<int64_t>(getInt(value, key));
}

std::unique_ptr<bool>
JsonManager::getOptionalBool(const Json::Value& value, std::string key) {
  if (value[key].isNull())
    return nullptr;

  return std::make_unique<bool>(getBool(value, key));
}

User JsonManager::readUser(const Json::Value& value) {
  if (value.isNull())
    Exception("json is null, but should be User\n");

  User user;
  user.first_name    = getString(value, "first_name");
  user.id            = getInt(   value, "id");
  user.is_bot        = getBool(  value, "is_bot");
  user.username      = getOptionalString(value, "username");
  user.last_name     = getOptionalString(value, "last_name");
  user.language_code = getOptionalString(value, "language_code");
  return user;
}

std::unique_ptr<User> JsonManager::readOptionalUser(const Json::Value& value) {
  if (value.isNull())
    return nullptr;

  return std::make_unique<User>(readUser(value));
}

Chat JsonManager::readChat(const Json::Value& value) {
  if (value.isNull())
    Exception("json is null, but should be Chat\n");

  Chat chat;
  chat.id         = getInt(value, "id");
  chat.type       = getString(value, "type");
  chat.first_name = getOptionalString(value, "first_name");
  chat.last_name  = getOptionalString(value, "last_name");
  return chat;
}

PhotoSize JsonManager::readPhotoSize(const Json::Value& value) {
  if (value.isNull())
    Exception("json is null, but should be PhotoSize\n");

  PhotoSize photoSize;
  photoSize.file_id   = getString(value, "file_id");
  photoSize.height    = getInt(value, "height");
  photoSize.width     = getInt(value, "width");
  photoSize.file_size = getOptionalInt(value, "file_size");
  return photoSize;
}

std::unique_ptr<PhotoSize>
JsonManager::readOptionalPhotoSize(const Json::Value& value) {
  if (value.isNull())
    return nullptr;

  return std::make_unique<PhotoSize>(readPhotoSize(value));
}

Sticker JsonManager::readSticker(const Json::Value& value) {
  if (value.isNull())
    Exception("json is null, but should be Sticker\n");

  Sticker sticker;
  sticker.file_id   = getString(value, "file_id");
  sticker.width     = getInt(value, "width");
  sticker.height    = getInt(value, "height");
  sticker.file_size = getOptionalInt(value, "file_size");
  sticker.emoji     = getOptionalString(value, "emoji");
  sticker.set_name  = getOptionalString(value, "set_name");
  sticker.thumb     = readOptionalPhotoSize(value["thumb"]);
  return sticker;
}

std::unique_ptr<Sticker> JsonManager::readOptionalSticker(const Json::Value& value) {
  if (value.isNull())
    return nullptr;

  return std::make_unique<Sticker>(readSticker(value));
}

Message JsonManager::readMessage(const Json::Value &value) {
  if (value["message"].isNull())
    Exception("json is null, but should be Message\n");

  if (value["update_id"].isNull())
    Exception("json is null, but should be int\n");

  const Json::Value &messageJson = value["message"];
  Message message;
  message.update_id  = getInt(value, "update_id");   // this should be value
  message.chat       = readChat(messageJson["chat"]);
  message.date       = getInt(messageJson, "date");
  message.message_id = getInt(messageJson, "message_id");
  message.from       = readOptionalUser(messageJson["from"]);
  message.text       = getOptionalString(messageJson, "text");
  message.sticker    = readOptionalSticker(messageJson["sticker"]);
  return message;
}

std::vector<Message> JsonManager::readArrayMessage(const Json::Value& value) {
  if (!value.isArray())
    Exception("json is not array");

  std::vector<Message> messages;
  for (int i = 0; !value[i].isNull(); ++i) {
    messages.push_back(readMessage(value[i]));
  }
  return messages;
}

Json::Value JsonManager::messageToJson(const SendingMessage& message) {
  Json::Value value;
  value["text"] = message.text;
  value["chat_id"] = message.chat_id;
  if (message.reply_to_message_id) {
    value["reply_to_message_id"] = *(message.reply_to_message_id);
  }
  return value;
}



