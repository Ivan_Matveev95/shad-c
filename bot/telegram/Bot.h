//
// Created by IVAN MATVEEV on 05.12.2017.
//

#pragma once

#include "BotApi.h"
#include <random>
#include <fstream>


class Bot {
public:
  explicit Bot(std::string server_url,
               std::string token,
               std::string first_name,
               std::string savingStateFilePath);
  ~Bot() = default;
  User GetMe();
  std::vector<Message> GetUpdates();
  std::vector<Message> GetUpdates(GettingUpdates update);
  void SendMessage(const SendingMessage& message);
  void Run();
  void Stop();
  void Random(const Message& message);
  void Weather(const Message& message);
  void StyleGuide(const Message& message);
  void Crash();

private:
  class RandomGenerator {
  public:
    int32_t rand();
    RandomGenerator();
  private:
    std::default_random_engine generator_;
    std::uniform_int_distribution<int32_t> distribution_;
  };

  class StateStorage {
  public:
    void SetOffset(int64_t offset);
    const GettingUpdates& GetGettingUpdate();
    StateStorage(std::string filePath, int64_t timeout);
    ~StateStorage();
  private:
    std::fstream file_;
    GettingUpdates update_;
  };

  BotApi botApi_;
  bool working_;
  StateStorage state_;
  RandomGenerator generator_;
  std::string first_name_;

  void Sticker(const Message& message);
};
