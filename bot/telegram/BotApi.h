//
// Created by IVAN MATVEEV on 03.12.2017.
//

#pragma once

#include <vector>
#include <memory>
#include <string>
#include <json/json.h>

struct User {
  std::string first_name;
  int64_t     id;
  bool        is_bot;
  std::unique_ptr<std::string> username;
  std::unique_ptr<std::string> last_name;
  std::unique_ptr<std::string> language_code;
};

struct Chat {
  int64_t     id;
  std::string type;
  std::unique_ptr<std::string> first_name;
  std::unique_ptr<std::string> last_name;
};

struct PhotoSize {
  std::string file_id;
  int64_t     width;
  int64_t     height;
  std::unique_ptr<int64_t> file_size;
};

struct Sticker {
  std::string file_id;
  int64_t     width;
  int64_t     height;
  std::unique_ptr<std::string> emoji;
  std::unique_ptr<std::string> set_name;
  std::unique_ptr<int64_t>     file_size;
  std::unique_ptr<PhotoSize>   thumb;
};

struct Message {
  int64_t update_id;
  int64_t message_id;
  int64_t date;
  Chat    chat;
  std::unique_ptr<User> from;
  std::unique_ptr<std::string> text;
  std::unique_ptr<Sticker> sticker;
  // gif
};

struct SendingMessage {
  int64_t chat_id;
  std::string text;
  std::unique_ptr<int64_t> reply_to_message_id;

  SendingMessage(int64_t in_chat_id, std::string in_text);
  SendingMessage(int64_t in_chat_id, std::string in_text, int64_t reply);
};

struct GettingUpdates {
  std::unique_ptr<int64_t> offset;
  std::unique_ptr<int64_t> timeout;
  std::unique_ptr<int64_t> limit;

  GettingUpdates() = default;
  GettingUpdates(int64_t in_timeout);
  GettingUpdates(int64_t in_timeout, int64_t in_offset);
  GettingUpdates(int64_t in_timeout, int64_t in_offset, int64_t in_limit);
};

{
  Json::Value::Int64 i;
}

class BotApi {
public:
  BotApi(std::string server_url, std::string token);
  ~BotApi();
  User GetMe();
  std::vector<Message> GetUpdates(const GettingUpdates& update);
  void SendMessage(const SendingMessage& message);

private:
  class BotApiImpl;

  std::unique_ptr<BotApiImpl> pimpl;
};


