//
// Created by IVAN MATVEEV on 05.12.2017.
//

#pragma once

#include "BotApi.h"
#include <json/json.h>
#include <string>



class JsonManager {
public:
  static std::string  getString(const Json::Value& value, std::string key);
  static int64_t      getInt(const Json::Value& value, std::string key);
  static bool         getBool(const Json::Value& value, std::string key);
  static std::unique_ptr<std::string> getOptionalString(const Json::Value& value, std::string key);
  static std::unique_ptr<int64_t>     getOptionalInt(const Json::Value& value, std::string key);
  static std::unique_ptr<bool>        getOptionalBool(const Json::Value& value, std::string key);

  static User      readUser(const Json::Value& value);
  static Chat      readChat(const Json::Value& value);
  static PhotoSize readPhotoSize(const Json::Value& value);
  static Sticker   readSticker(const Json::Value& value);
  static Message   readMessage(const Json::Value& value);
  static std::unique_ptr<PhotoSize> readOptionalPhotoSize(const Json::Value& value);
  static std::unique_ptr<User>      readOptionalUser(const Json::Value& value);
  static std::unique_ptr<Sticker>   readOptionalSticker(const Json::Value& value);

  static std::vector<Message> readArrayMessage(const Json::Value& value);
  static Json::Value messageToJson(const SendingMessage& message);

};


